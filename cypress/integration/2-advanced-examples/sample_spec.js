// describe ('customhook test'() => {
//   it ('id test',()=> {
//     cy.visit('http://localhost:3000/customhook');
//     cy.get('1')
//   } )
// })

describe('interface test', ()=>{
   beforeEach(()=>{
       cy.visit('http://localhost:3000/customhook')
   })
   it('Enter and Button Test', ()=>{
       cy.get('.submitBtn').click();
   })
   it('Check Delete', ()=>{
       cy.get('.todoInput').click()
       .should('be.visible')
   })
})

describe('customhook test', ()=>{
   beforeEach(()=>{
       cy.visit('http://localhost:3000/customhook')
   })
   it('Interact with Elements using ID selector', ()=>{
       cy.get('.col-12 > :nth-child(1)')
       .should('be.visible')
   })
})

describe('date component works', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/customhook')
  })

  context('calendar opens on click', () => {
    beforeEach(() => {
      // open the calendar modal
      // StackBlitz takes a while to load, thus we need
      // to give "cy.get" a longer timeout for the element to appear
      cy.get('.submitBtn').click()
      cy.get('.DayPickerInput > input').click({force: true})
    })

    it('calendar auto closes on click', () => {
      // const day = 5
      // const dayRegex = new RegExp(`^\\b${day}\\b$`)

      cy.get('.DayPickerInput > input')
        .should('be.visible')
        .get('.DayPicker-Body > :nth-child(1) > [tabindex="0"]')
        .click()

      // calendar should close on the user click
      cy.get('.DayPickerInput > input').should('be.visible')
    })
  })
})
