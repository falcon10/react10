import React, {useEffect} from 'react';
import {useGet} from 'restful-react'

const Image = ({breed}) => {
  const {data, refetch, loading} = useGet({
    path:`https://dog.ceo/api/breed/${breed}/images/random`,
  });

  if (loading) {
    return <h2>Loading</h2>;
  }

  return (
    <div>
       <img onClick={refetch} alt="doggy1" src={data?.message} width='300px' />
    </div>
  )
}

const Test = () => {
return (
    <div>
       <Image breed={'bulldog'} />
       <Image breed={'african'} />
       <Image breed={'eskimo'} />
       <Image breed={'boxer'} />
    </div>
  )
}

// const Test = () => {
//   const {data: african, refetch: firstRefetch, loading: firstLoader, error: firstError} = useGet({
//     path:"https://dog.ceo/api/breed/african/images/random",
//   })
//   const {data: bulldog, refetch: secondRefetch, loading: secondLoader, error: secondError} = useGet({
//     path:"https://dog.ceo/api/breed/bulldog/images/random",
//   })
//   const {data: eskimo, refetch: thirdRefetch, loading: thirdLoader, error: thirdError} = useGet({
//     path:"https://dog.ceo/api/breed/eskimo/images/random",
//   })
//   const {data: boxer, refetch: fourthRefetch, loading: fourthLoader, error: fourthError} = useGet({
//     path:"https://dog.ceo/api/breed/boxer/images/random",
//   })
//
//
// //   useEffect(() => {
// //   if ((message) ==! null) {
// //     <img onClick={() => refetch()} alt="doggy1" src={affenpinscher?.message} width="300px"/>
// //   }
// //   else {
// //   }
// // }, []);
// //
// //   useEffect(() => {
// //   if (loading && null) {
// //     <img onClick={() => refetch()} alt="doggy1" src={african && eskimo?.message} width="300px"/>
// //   }
// //   else {
// //   }
// // }, []);
//
//   console.log(african)
//   console.log(bulldog)
//   console.log(eskimo)
//   console.log(boxer)
//
// return (
//   <div>
// {firstLoader ? <h1>loading...</h1> : <div><img onClick={firstRefetch} alt="doggy1" src={african?.message} width="300px"/></div>}
// {secondLoader ? <h1>loading...</h1> : <div><img onClick={secondRefetch} alt="doggy2" src={bulldog?.message} width="300px"/></div>}
// {thirdLoader ? <h1>loading...</h1> : <div><img onClick={thirdRefetch} alt="doggy3" src={eskimo?.message} width="300px"/></div>}
// {fourthLoader ? <h1>loading...</h1> : <div><img onClick={fourthRefetch} alt="doggy4" src={boxer?.message} width="300px"/></div>}
// </div>
// )
//
// }

export default Test
