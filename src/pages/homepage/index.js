import React,{useState} from 'react';
import components, {lazyComponents} from '../../components';
import ComponentA from "../../components/Inputs/hocClassComponent/ComponentA";
import ComponentB from "../../components/Inputs/hocFunctionalComponent/ComponentB";
import Name from '../../components/Inputs/name/index.js'
// import CustomName from '../../components/Inputs/name/index0'
import Eman from '../../components/Inputs/name/index0'
import WrappedComponent from "../../components/Inputs/hocFunctionalComponent/index.js";



function Homepage() {
const [fname, setFname] = useState("")
const handleChange = e => {
 setFname(e.target.value)
}

  return (
    <div>
    <div className="col-6">
     <ComponentA />
    </div>
     <div className="col-7">
     <Eman />
    </div>
    </div>
  );
}

export default Homepage

//
// const Homepage = () => {
//   const {Test} = components;
//   const [txt,addTxt] = useState('');
//
// return (
//   <div>
//   <Test {...{txt}} />
//   <label>Text:</label>
//   <input name="name0" type="text" value={txt} onChange={(e)=>addTxt(e.target.value)}/>
//   </div>
// );
// };
//
// export default Homepage;
