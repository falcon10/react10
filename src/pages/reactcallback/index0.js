import React, {Component, useState, useEffect, useCallback} from 'react';
import styled from 'styled-components';
import RectangleResult from './CallbackResult';
import { keyframes } from "styled-components";
import Animate from "react-move/Animate";
import _ from 'lodash';

// let width = 300,
//   height = 300;
// let colours = ["#2176ae", "#57b8ff", "#b66d0d", "#fbb13c", "#fe6847"];
//
// function getData() {
//   return {
//     pos: {
//       x: width * Math.random(),
//       y: height * Math.random()
//     },
//     r: 10 + 10 * Math.random(),
//     colour: Math.floor(5 * Math.random())
//   };
// }
//
//
// class App extends Component {
//
// // Create state
// state = {
// 	xoffset: -20,
// 	yoffset: -40,
// 	delta: 10,
// };
//
// moveTitleToDown = () => {
// 	this.setState(
// 	{ yoffset: this.state.yoffset
// 		+ this.state.delta });
// };
// moveTitleToRight = () => {
// 	this.setState(
// 	{ xoffset: this.state.xoffset
// 		+ this.state.delta });
// };
// moveTitleToLeft = () => {
// 	this.setState(
// 	{ xoffset: this.state.xoffset
// 		- this.state.delta });
// };
// moveTitleToUp = () => {
// 	this.setState(
// 	{ yoffset: this.state.yoffset
// 		- this.state.delta });
// };
//
// render() {
// 	return (
//     <div style={{
//     marginTop: "-180px",
//     width: "300 px",
//     border: "15px solid green",
//     padding: "50px",
//     margin: "20px" }}>
// 		{/* Element to Move Dynamically */}
// 		<h2
// 		style  class="circle move-1" style={{
// 			position: "absolute",
// 			left: `${this.state.xoffset}px`,
// 			top: `${this.state.yoffset}px`,
//       color: "red"
// 		}}
// 		>
// 		GeeksforGeeks
// 		</h2>
//
// 		{/* Move Controls */}
// 		<div style={{
//     marginTop: "-80px",
//     width: "300 px",
//     border: "15px solid green",
//     padding: "50px",
//     margin: "20px" }}>
// 		<button onClick={this.moveTitleToRight}>
// 			Move Title To Right
// 		</button>
// 		<button onClick={this.moveTitleToDown}>
// 			Move Title To Down
// 		</button>
// 		<button onClick={this.moveTitleToLeft}>
// 			Move Title To Left
// 		</button>
// 		<button onClick={this.moveTitleToUp}>
// 			Move Title To Up
// 		</button>
// 		</div>
// 	</div>
// 	);
// }
// }
//
// export default App;

let width = 50,
  height = 30;
let colours = ["#2176ae", "#57b8ff", "#b66d0d", "#fbb13c", "#fe6847"];

function getData() {
  return {
    pos: {
      x: width * Math.random(),
      y: height * Math.random()
    },
    r: 10 + 10 * Math.random(),
    colour: Math.floor(5 * Math.random())
  };
}

class App extends Component {



  constructor(props) {
    super(props);

    this.state = {
      data: getData(),
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      data: getData(),
    });
  }

  render() {
    let d = this.state.data;

    return (
      <div>
      <div class="card">
      <div class="box"></div>
      </div>
      <button onClick={this.handleClick}>Update</button>
      </div>
    );
  }
}

export default App;
//
// function makeNewPosition(){
//
//     // Get viewport dimensions (remove the dimension of the div)
//     var h = (window).height() - 50;
//     var w = (window).width() - 50;
//
//     var nh = Math.floor(Math.random() * h);
//     var nw = Math.floor(Math.random() * w);
//
//     return [nh,nw];
//
// }
// //
// const useMouse = (initialValue = { x:0, y:0 }) => {
//   const [position, setPosition] = useState(initialValue);
//   const handleMouseMove = e => {
//     setPosition({
//       x: (e.clientX+28),
//       y: (e.clientY-28)
//     });
//   };
//   return [position, handleMouseMove];
// };
//
// function App() {
//   const [yOffset, setYOffset] = useState(200);
//   const [xOffset, setXOffset] = useState(10);
//   const [isFollowing, setIsFollowing] = useState(false);
//   const [position, handleMouseMove] = useMouse();
//   const [min,max] = useState(10);
//
//   const moveBox=(e)=> {
//   // const { name } = e.target;
//   setYOffset(e.clientY+120,e.clientY-120);
//   setXOffset(e.clientX-30,e.ClientX+( Math.random(Math.random()*(max-min))+min + 1));
//   // setWidth( Math.random(Math.random()*(max-min))+min + 1); setHeight (height +1); setVolume(width*height)
//   }
//   //
//   // useEffect ((e)=>{
//   //   setYOffset(e.clientY+120,e.clientY-120);
//   //   setXOffset(e.clientX-30,e.ClientX+( Math.random(Math.random()*(max-min))+min + 1));
//   //   },[])
//
//
//   // const moveBox = e => {
//   //   const { name } = e.target;
//   //   return {
//   //     up: () => setYOffset(e.clientY+28),
//   //     down: () => setYOffset(e.clientY+28),
//   //     left: () => setXOffset(e.clientX-28),
//   //     right: () => setXOffset(e.clientX+28)
//   //   }[name]();
//   // };
//
//   // const toggleFollow = () => {
//   //   setIsFollowing(!isFollowing);
//   // };
//
//   const styles = {
//     box: {
//       backgroundColor: "#cc0303",
//       height: "20px",
//       width: "20px",
//       position: "absolute",
//       left: xOffset,
//       top: yOffset
//     }
//   };
//
//   return (
//     <div className="App">
//       <h1>Move the Box!</h1>
//       <button name="move" onClick={moveBox}>
//       Move
//       </button>
//       <div className="box" style={styles.box} />
//     </div>
//   );
// }
//
// export default App

// const CallbackResult = ({elPos, reverse}) => {
//    const testFunction = () => {
//      console.log(elPos)
//    }
//
// useEffect(() => {
//   testFunction();
//   console.log('test function changed')
// },[reverse])
//
// return (
//   <div>
//    <p>top: {elPos.top}</p>
//    <p>top: {elPos.left}</p>
//    <button onClick={()=>testFunction()}>Check Function></button>
//   </div>
// )
// };
// export default CallbackResult

// export const MyRectangle = styled.div`
// color: red;
// width: ${props => props.width +'px'};
// height: ${props => props.height +'px'};
// background-color: black;
// display: flex;
// text-align: center;
// align-items: center;
// justify-content: center;
// transition: all 0.5s;
// }`
//
// function Rectangle1 () {
//
// const[width]=useState(103);
// const[height]=useState(105);
// const[volume]=useState(width*height)
//
// return (
//  <div className = "rectangle__wrapper">
//    <RectangleResult {...volume}/>
//  </div>
//
//
// )
// }
//
// export default function App() {
//   return (
//     <div>
//       <Rectangle1 />
//     </div>
//   );
// }

// class Rectangle extends Component {
// constructor() {
// super()
// this.state = {
// width: 103,
// height: 120,
// max: 100,
// min: 100,
// }
//
// this.handleResize = this.handleResize.bind(this)
// }
//
//
//
// handleResize = () => {
// const max = this.state.max;
// const min = this.state.min;
// let width = this.state.width;
// let height = Math.random(Math.random()*(max-min))+min;
// let olo = this.state.width*this.state.height;
// this.setState({
// width: this.state.width + 1,
// height: Math.random(Math.random()*(max-min))+min,
// olo: width*height
// })
// }
//
// render() {
// let {width, height, olo} = this.state;
// const ref = React.createRef();
// return (
//  <div className = "rectangle__wrapper">
//    <MyRectangle ref={ref}
//      width={width}
//      height={height}
//      olo={olo}
//    >
//    <RectangleResult {...{ref}} />
//    </MyRectangle>
//    <p>Szerokość:{width} px</p>
//    <p>Wysokość:{height} px</p>
//    <p>Olo:{olo} px</p>
//    <button onClick={()=>this.handleResize()}>Generuj nowy element</button>
//  </div>
//
// )
//
// }
// }

// export default Rectangle
