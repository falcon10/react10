import React, {useEffect, useCallback, useState} from 'react';

export default function App() {

  const [top] = useState(0);
  const [left] = useState(0);
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);
  const [directionH, setDirectionH] = useState(true);
  const [directionV, setDirectionV] = useState(true);
  const maxLeft = 300;
  const maxTop = 300;


 const colors = [
 ["yellow",
 "green"],
 ["blue",
 "pink"],
 ["violet",
 "chocolate"],
 ["grey",
 "black"],
 ["purple",
 "red"]]


const [actualColor, setActualColor] = useState('')

 useEffect(() => {

   if (x >= 150 && setDirectionH) {
     setActualColor( colors[Math.floor(Math.random()* (colors.length-1))][1])
     console.log('updating items')
   }
   else if  (x <= 150 && setDirectionH) {
     setActualColor( colors[Math.floor(Math.random() * (colors.length-1))][0])
     console.log('updating items')
  }

},[ x <= 150, x >= 150, directionH])


  useEffect(() => {
    if (x > (maxLeft - 50)) {
      setDirectionH(false)
      }
      else if (x < 50){
      setDirectionH(true)
     }

    if (y > maxTop - 50){
      setDirectionV(false)
      }
      else if (y < 25){
      setDirectionV(true)
      }
  }, [x, y])

  const updateColors = useCallback(
         () => {
            if (directionH) {
              setActualColor( colors[Math.floor(Math.random()*(colors.length-1))][1])
              console.log('updating items usecallback')
            }
            else if (setDirectionH) {
              setActualColor( colors[Math.floor(Math.random() * (colors.length-1))][0])
              console.log('updating items usecallback')
           }
         },
         [directionH, setDirectionH]
     );

  const changePositions = () => {
    if (directionH) {
      setX((x) => x + 20);
    } else {
      setX((x) => x - 20);
    }
    if (directionV) {
      setY((y) => y + 45);
    } else {
      setY((y) => y - 45);
    }
  }


  const styles = {
    box: {
      backgroundColor: actualColor,
      borderRadius: 50,
      height: "20px",
      width: "20px",
      position: "absolute",
      left: x,
      top: y,
    },
  }
  return (
    <div className="App">
      <button name="button" onClick={changePositions}>
        Positions
      </button>
      <div className="card">
        <div className="box" style={styles.box} />
      </div>
      <div>
      <button onClick={updateColors}>Colors</button>
      </div>
    </div>
  );
}
