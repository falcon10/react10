import React, {Component, useState, useRef } from 'react';
import CallbackResult from './index.js';
import styled from 'styled-components';

class App extends Component {

// Create state
state = {
	xoffset: 0,
	yoffset: 0,
	delta: 10,
};

moveTitleToDown = () => {
	this.setState(
	{ yoffset: this.state.yoffset
		+ this.state.delta });
};
moveTitleToRight = () => {
	this.setState(
	{ xoffset: this.state.xoffset
		+ this.state.delta });
};
moveTitleToLeft = () => {
	this.setState(
	{ xoffset: this.state.xoffset
		- this.state.delta });
};
moveTitleToUp = () => {
	this.setState(
	{ yoffset: this.state.yoffset
		- this.state.delta });
};

render() {
	return (
	<div>
		<h2
		style={{
			position: "absolute",
			left: `${this.state.xoffset}px`,
			top: `${this.state.yoffset}px`,
		}}
		>
		GeeksforGeeks
		</h2>

		{/* Move Controls */}
		<div style={{ marginTop: "80px" }}>
		<button onClick={this.moveTitleToRight}>
			Move Title To Right
		</button>
		<button onClick={this.moveTitleToDown}>
			Move Title To Down
		</button>
		<button onClick={this.moveTitleToLeft}>
			Move Title To Left
		</button>
		<button onClick={this.moveTitleToUp}>
			Move Title To Up
		</button>
		</div>
	</div>
	);
}
}

export default App;


// export const MyRectangle = styled.div`
// color: red;
// width: ${props => props.width +'px'};
// height: ${props => props.height +'px'};
// background-color: black;
// display: flex;
// text-align: center;
// align-items: center;
// justify-content: center;
// transition: all 0.5s;
// }`
//
// export default function RectangleResult() {
//   const inputRef = useRef();
//   const[width, setWidth]=useState(103);
//   const[min, max]=useState(100);
//   const[height, setHeight]=useState(105);
//   const[volume, setVolume]=useState(width*height)
//
//   const handleResize=()=> {
//   setWidth( Math.random(Math.random()*(max-min))+min + 1); setHeight (height +1); setVolume(width*height)
//   }
//
//   return (
//      <div className = "rectangle__wrapper">
//       <CallbackResult ref={inputRef} width={width} height={height}>{volume}</CallbackResult>
//
//       <p>Szerokość:{width} px</p>
//       <p>Wysokość:{height} px</p>
//       <button onClick={()=>handleResize()}>Generuj nowy element</button>
//     </div>
//   );
// }

// export const StyledRectangle = styled.div`
//   color: red;
//   font-weight: bold;
//   width: ${props => props.width +'px'};
//   height: ${props => props.height +'px'};
//   background-color: green;
//   display: flex;
//   text-align: center;
//   align-items: center;
//   justify-content: center;
//   transition: all 0.5s;
//   `;
// export function handleResize() {
//         const[width, setWidth]=this.state(103);
//         const[min, max]=this.state(100);
//         const[height, setHeight]=this.state(105);
//         const[volume, setVolume]=this.state(width*height)
//
//       setWidth( Math.random(Math.random()*(max-min))+min + 1); setHeight (height +1); setVolume(width*height)
// }

// function RectangleResult() {
//
//     const[width, setWidth]=useState(103);
//     const[min, max]=useState(100);
//     const[height, setHeight]=useState(105);
//     const[volume, setVolume]=useState(width*height)
//
//     function onClickFunc() {
//
//         setWidth( Math.random(Math.random()*(max-min))+min + 1); setHeight (height +1); setVolume(width*height)}
//
//           const inputRef = useRef()
//
//     return (
//       <div className="App">
//
//       <StyledRectangle width={width}height={height}>{volume}</StyledRectangle>
//       <p>Szerokość:{width} px</p>
//       <p>Wysokość:{height} px</p>
//       <button onClick={()=>onClickFunc()}>Generuj nowy element</button>
//       </div>
// )}



// export default function UseRefExample

// export default function Button ({ label, action }) {
//         // declare & initializing a reference to null
//    const buttonRef = useRef(null)
//
//    // attaching 'buttonRef' to the <button> element in JSX
//     return (
//       <button onClick={action} ref={buttonRef}>{label}</button>
//     )
//   }

// const Button = ({ close }) => {
//     const [value, updateVal] = useState("");
//     const onChange = (e) => {
//       updateVal(e.target.value);
//     };
//
//     const onSubmit = (e) => {
//       e.preventDefault();
//       close();
//     };
//

//     const handleResize=(e)=> {
//     e.preventDefault();
//     setWidth( Math.random(Math.random()*(max-min))+min + 1);
//     setHeight (height +1);
//     setVolume(width*height);
//     close()
//     }
    // const App = () => {
    //   const[width, setWidth]=useState(103);
    //   const[min, max]=useState(100);
    //   const[height, setHeight]=useState(105);
    //   const[volume, setVolume]=useState(width*height)
    //   const buttonRef = useRef();
    //   return(
    //     <div className="box">
    //       <h1>The Button has a ref</h1>
    //       <button
    //         onClick={() => {
    //           setWidth( Math.random(Math.random()*(max-min))+min + 1);
    //           setHeight (height +1);
    //           setVolume(width*height);
    //         }}
    //         ref={buttonRef}
    //         className="button">
    //           Special Button
    //        </button>
    //     </div>
    //   );
    // }
