import {useEffect} from 'react';
import { useLocalstorage } from "rooks";
import useTodoList from "./usetodolist";
import TodoItem from "./todoitem";
import Form from "./form";

export default function App() {
  const { todos, addTodo, deleteTodo, updateTodo } = useTodoList();

  return (
  <div>
    <div className="layout">
      <div className = "col-12 flex_wrapper">
        <p className = "col-2"> ID </p>
        <p className = "col-2"> Nazwa </p>
        <p className = "col-2"> Wykonać do </p>
        <p className = "col-2"> Wykonane? </p>
        <p className = "col-2"> Usuń </p>
      </div>
        <div className="todoList">
          {todos.map((todo) => (
            <TodoItem
              key={todo.id}
              todo={todo}
              deleteTodo={deleteTodo}
              updateTodo={updateTodo}
            />
          ))}
        </div>
        <Form addTodo={addTodo} />
      </div>
  </div>
  );
  };
