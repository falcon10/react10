import { useState } from "react";

const useTodoList = () => {
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    setTodos([...todos, todo]);
  };

  const deleteTodo = (id) => {
    const newTodos = todos.filter((todo) => todo.id !== id);

    setTodos(newTodos);
  };

  const updateTodo = (id) => {
    const targetTodoIndex = todos.findIndex((todo) => todo.id === id);
    const newTodos = [...todos];

    newTodos[targetTodoIndex] = {
      ...newTodos[targetTodoIndex],
      done: !newTodos[targetTodoIndex].done
    };

    setTodos(newTodos);
  };

  return { todos, addTodo, deleteTodo, updateTodo };
};

export default useTodoList;
