import { useState, useEffect } from "react";
import { useLocalstorage } from "rooks";


export default function Form({ addTodo }) {
  const [name, setName] = useState('');

  const submitForm = (e) => {

    e.preventDefault();
    localStorage.setItem('name', name);
    localStorage.getItem('name')
    const newTodo = { id: Math.random(), name, done: false };
    addTodo(newTodo);
    setName("");
  };


  return (
    <div>
      <span>Add to the todo list</span>
      <div>
        <input
          type="input"
          className="todoInput"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <button type="button" className="submitBtn" onClick={submitForm}>
          Click me
        </button>
      </div>
    </div>
  );
}
