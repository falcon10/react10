import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

export default function TodoItem({ todo, updateTodo, deleteTodo }) {
  return (
    <div className="todoItem">
      <div>
      <span className = "col-2"
        style={{
          textDecoration: todo.done ? "line-through" : "none"
        }}
      >
        {todo.id}
      </span>
      <span className = "col-2"
        style={{
          textDecoration: todo.done ? "line-through" : "none"
        }}
      >
        {todo.name}
      </span>
      <span className = "col-2"
        style={{
          textDecoration: todo.done ? "line-through" : "none"
        }}
      >
         <DayPickerInput onDayChange={day => console.log(day)} />
      </span>
        <input className = "col-3"
          type="checkbox"
          checked={todo.done}
          onChange={() => {
            updateTodo(todo.id);
          }}
        />
        <button className = ""
          type="button"
          onClick={() => {
            deleteTodo(todo.id);
          }}
        >
          Remove
        </button>
      </div>
    </div>
  );
}
