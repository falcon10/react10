import React, {useState} from 'react';
import styled from 'styled-components';
import RectangleResult from './rectangleResult';

export const MyCircle = styled.div`
color: red;
width: ${props => props.width +'px'};
height: ${props => props.height +'px'};
background-color: black;
display: flex;
text-align: center;
align-items: center;
justify-content: center;
transition: all 0.5s;
border-radius: 50%;
}`


export const MyRectangle = styled.div`
color: red;
width: ${props => props.width +'px'};
height: ${props => props.height +'px'};
background-color: black;
display: flex;
text-align: center;
align-items: center;
justify-content: center;
transition: all 0.5s;
}`


export default function App() {
  return (
    <div className="App">
      <HOComponent AppComponent={Rectangle} />
    </div>
  );
}

function HOComponent({ AppComponent }) {
  return (
    <div className="HOComponent__wrapper">
      <Rectangle />
    </div>
  );
}


function Rectangle () {

const[width]=useState(103);
const[height]=useState(105);
const[volume]=useState(width*height)

return (
 <div className = "rectangle__wrapper">
   <RectangleResult {...volume}/>
 </div>


)
}

// class Rectangle extends Component {
// constructor() {
// super()
// this.state = {
// width: 103,
// height: 120,
// max: 100,
// min: 100,
// }
//
// this.handleResize = this.handleResize.bind(this)
// }
//
//
//
// handleResize = () => {
// const max = this.state.max;
// const min = this.state.min;
// let width = this.state.width;
// let height = Math.random(Math.random()*(max-min))+min;
// let olo = this.state.width*this.state.height;
// this.setState({
// width: this.state.width + 1,
// height: Math.random(Math.random()*(max-min))+min,
// olo: width*height
// })
// }
//
// render() {
// let {width, height, olo} = this.state;
// const ref = React.createRef();
// return (
//  <div className = "rectangle__wrapper">
//    <MyRectangle ref={ref}
//      width={width}
//      height={height}
//      olo={olo}
//    >
//    <RectangleResult {...{ref}} />
//    </MyRectangle>
//    <p>Szerokość:{width} px</p>
//    <p>Wysokość:{height} px</p>
//    <p>Olo:{olo} px</p>
//    <button onClick={()=>this.handleResize()}>Generuj nowy element</button>
//  </div>
//
// )
//
// }
// }

// export default Rectangle
