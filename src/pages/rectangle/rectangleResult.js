import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import {MyRectangle, MyCircle} from './index.js'

export default function RectangleResult() {
  const inputRef = useRef();
  const[width, setWidth]=useState(104);
  const[min, max]=useState(100);
  const[height, setHeight]=useState(109);
  const[volume, setVolume]=useState(width*height)
  const[borderRadius, setBorderRadius]=useState(0)

  const handleResize=()=> {
  if(height>=113 && width<=113) {
    setWidth(width + 2); setHeight (height + 1); setVolume(width*height);
    setBorderRadius(300)
 }
  else {setWidth(width + 2); setHeight (height + 1); setVolume(width*height); setBorderRadius(0)}

  }

//   useEffect(() => {
//       if(height<=110) {
//         setWidth(110)
//         setVolume(3.14*width/2*2)
//
//       }
// },[]);
// <MyRectangle ref={inputRef} width={width} height={height}>{volume}</MyRectangle>
// <MyCircle ref={inputRef} width={width} height={height}>{volume}</MyCircle>
  return (
     <div className = "rectangle__wrapper">
     <MyRectangle ref={inputRef} style={{borderRadius: borderRadius + 'px'}} width={width} height={height}>{volume}</MyRectangle>
      <p>Szerokość:{width} px</p>
      <p>Wysokość:{height} px</p>
      <button onClick={()=> handleResize()}>Generuj nowy element</button>
    </div>
  );
}

// export const StyledRectangle = styled.div`
//   color: red;
//   font-weight: bold;
//   width: ${props => props.width +'px'};
//   height: ${props => props.height +'px'};
//   background-color: green;
//   display: flex;
//   text-align: center;
//   align-items: center;
//   justify-content: center;
//   transition: all 0.5s;
//   `;
// export function handleResize() {
//         const[width, setWidth]=this.state(103);
//         const[min, max]=this.state(100);
//         const[height, setHeight]=this.state(105);
//         const[volume, setVolume]=this.state(width*height)
//
//       setWidth( Math.random(Math.random()*(max-min))+min + 1); setHeight (height +1); setVolume(width*height)
// }

// function RectangleResult() {
//
//     const[width, setWidth]=useState(103);
//     const[min, max]=useState(100);
//     const[height, setHeight]=useState(105);
//     const[volume, setVolume]=useState(width*height)
//
//     function onClickFunc() {
//
//         setWidth( Math.random(Math.random()*(max-min))+min + 1); setHeight (height +1); setVolume(width*height)}
//
//           const inputRef = useRef()
//
//     return (
//       <div className="App">
//
//       <StyledRectangle width={width}height={height}>{volume}</StyledRectangle>
//       <p>Szerokość:{width} px</p>
//       <p>Wysokość:{height} px</p>
//       <button onClick={()=>onClickFunc()}>Generuj nowy element</button>
//       </div>
// )}



// export default function UseRefExample

// export default function Button ({ label, action }) {
//         // declare & initializing a reference to null
//    const buttonRef = useRef(null)
//
//    // attaching 'buttonRef' to the <button> element in JSX
//     return (
//       <button onClick={action} ref={buttonRef}>{label}</button>
//     )
//   }

// const Button = ({ close }) => {
//     const [value, updateVal] = useState("");
//     const onChange = (e) => {
//       updateVal(e.target.value);
//     };
//
//     const onSubmit = (e) => {
//       e.preventDefault();
//       close();
//     };
//

//     const handleResize=(e)=> {
//     e.preventDefault();
//     setWidth( Math.random(Math.random()*(max-min))+min + 1);
//     setHeight (height +1);
//     setVolume(width*height);
//     close()
//     }
    // const App = () => {
    //   const[width, setWidth]=useState(103);
    //   const[min, max]=useState(100);
    //   const[height, setHeight]=useState(105);
    //   const[volume, setVolume]=useState(width*height)
    //   const buttonRef = useRef();
    //   return(
    //     <div className="box">
    //       <h1>The Button has a ref</h1>
    //       <button
    //         onClick={() => {
    //           setWidth( Math.random(Math.random()*(max-min))+min + 1);
    //           setHeight (height +1);
    //           setVolume(width*height);
    //         }}
    //         ref={buttonRef}
    //         className="button">
    //           Special Button
    //        </button>
    //     </div>
    //   );
    // }
