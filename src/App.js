import './App.css';
import Homepage from './pages/homepage';
import {GameApp} from './context';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import Character from './pages/character';
import Game from './pages/game';
import Rectangle from './pages/rectangle';
import BouncingBall from './pages/reactcallback';
import ToDoList from './pages/customhook';
import Test from './pages/restfulapi';
import Demo from './pages/carousell';

require ('./hoc');

function App() {
  return (
    <div>
    <button onClick={event =>  window.location.href='/homepage'}>Start the game</button>
    <p></p>

    <Router>
    <GameApp>
         <li>
            <Link to="/homepage">Home</Link>
         </li>
         <li>
            <Link to="/character">Postać</Link>
         </li>
         <li>
            <Link to="/game">Świat gry</Link>
         </li>
         <li>
            <Link to="/rectangle">Rectangle</Link>
         </li>
         <li>
            <Link to="/reactcallback">Ball</Link>
         </li>
         <li>
            <Link to="/customhook">CustomHook</Link>
         </li>
         <li>
            <Link to="/restfulapi">Restfulapi</Link>
         </li>
         <li>
            <Link to="/carousell">Carousell</Link>
         </li>
     <Routes>
      <Route exact path='/homepage' element={< Homepage />}> </Route>
      <Route exact path='/character' element={< Character />}> </Route>
      <Route exact path='/game' element={< Game />}> </Route>
      <Route exact path='/rectangle' element={< Rectangle />}> </Route>
      <Route exact path='/reactcallback' element={< BouncingBall />}> </Route>
      <Route exact path='/customhook' element={< ToDoList />}> </Route>
      <Route exact path='/restfulapi' element={< Test />}> </Route>
      <Route exact path='/carousell' element={< Demo />}> </Route>
      </Routes>
      </GameApp>
      </Router>
    </div>
  );
}

export default App;
