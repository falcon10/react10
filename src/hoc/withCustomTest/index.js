import CustomTxt from './customTxt'


const withCustomText = Component => props => {
  return(
    <div>
    {
      props.txt === 'aaa'
      ? <CustomTxt {...props} />
      : <Component {...props}/>
    }
    </div>
  )
};




export default withCustomText
