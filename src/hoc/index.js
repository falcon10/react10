import components from '../components';
import Test from '../components/testComponent';
import withCustomTest from './withCustomTest';

components.Test = withCustomTest(props => <Test {...props} />)
