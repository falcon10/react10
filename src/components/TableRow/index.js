import React, {useState} from 'react';
import ButtonIncrement from '../../components/Buttons';
import ButtonDecrement from '../../components/Buttons/ButtonDecrement.js';

const TableRow = props => {

  const [count, setCounter] = useState(20);
  const incrementCounter = () => setCounter(count + 1);
  const decrementCounter = () => setCounter(count - 1);

  return (
   <>
   <table>
     <tbody>
     <ButtonIncrement onClickFunc={incrementCounter}/>
     <ButtonDecrement onClickFunc={decrementCounter}/>
       <tr><td>Nick:{props.name}</td></tr>
       <tr><td>Siła:{props.str}</td></tr>
       <tr><td>Zycie:{props.hp}</td></tr>
       <tr><td>Prędkość atk.:{props.speed}</td></tr>
       <tr><td>Obrażenia:{count}</td></tr>
      </tbody>
    </table>
    <div>
    <div>
      <p>Poziom: {props.lvl}</p>
    </div>
    <div>
      <div></div>
    </div>
    </div>
    </>
  )
}

export default TableRow
