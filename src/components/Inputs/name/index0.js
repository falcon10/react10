import React, {useState} from 'react';


const Eman = (props) => {

  const [fname, setFname] = useState("")

  const handleChange = e => {
   setFname(e.target.value)
  }

  return (
  (fname==='2')
  ? <div>
    <input type="text" value={fname} onChange={handleChange} />
    <h1>HOC: {fname}</h1>
    </div>
  :
    <div>
    <input type="text" value={fname} onChange={handleChange} />
    <h1>Wprowadź tekst: {fname}</h1>
    </div>

  )
}
export default Eman

// class Eman extends React.Component {
//   render() {
//     return (
//       <div className="">
//         <p>Component D</p>
//       </div>
//     );
//   }
// }

// export default Eman;
