import React from 'react';
import WrappedComponent from "../../Inputs/hocClassComponent/WrappedComponent";

export default class Name extends React.Component {
    constructor(props) {
      super(props);
      this.state = {text: '', inputText: '', mode:''};

      this.handleChange = this.handleChange.bind(this);
      this.handleSave = this.handleSave.bind(this);
      this.handleEdit = this.handleEdit.bind(this);
    }

  handleChange(e) {
    this.setState({ inputText: e.target.value });
  }

  handleSave() {
    this.setState({text: this.state.inputText, mode: 'view'});
  }

  handleEdit() {
    this.setState({mode: 'edit'});
  }

  render () {
    if(this.state.inputText === 'view') {
      return (
        <div>
          <p>HOC: {this.state.inputText}</p>
            <input
              onChange={this.handleChange}
              value={this.state.inputText}
            />
        </div>
      );
    } else {
      return (
        <div>
          <p>Text: {this.state.inputText}</p>
            <input
              onChange={this.handleChange}
              value={this.state.inputText}
            />
        </div>
      );
    }
  }
  }
