import React, { Component, useState } from "react";
import WrappedComponent from "../../Inputs/hocClassComponent/WrappedComponent";
import Eman from '../../Inputs/name/index0'

function ComponentB() {

  const [fname, setFname] = useState("")

  const handleChange = e => {
   setFname(e.target.value)
  }

  return (
    (ComponentB)
    ? <div>
    <input type="text" value={fname} onChange={handleChange} />
    <h1>Wprowadź imię: {fname}</h1>
    </div>
    :
    <div>
    <input type="text" value={fname} onChange={handleChange} />
    <h1>Wprowadź imię: {fname}</h1>
    </div>
  )
}

export default WrappedComponent("Home")(ComponentB);
