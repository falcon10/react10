import React, { Component } from "react";
import Name from '../../Inputs/name/index.js';
import CustomName from '../../Inputs/name/index0';
import Eman from '../../Inputs/name/index0'

export default withTitle => WrappedComponent =>
  class C extends Component {
      constructor(props) {
        super(props);
        this.state = {text: '', inputText: '', mode:''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
      }

      handleChange(e) {
        this.setState({ inputText: e.target.value });
      }

      handleSave() {
        this.setState({text: this.state.inputText, mode: 'view'});
      }

      handleEdit() {
        this.setState({mode: 'edit'});
      }

    render () {
      if(this.state.inputText === 'view') {
      } else {
        return (
          <div>
            <Name {...this.state.text} />
          </div>
        );
      }
    }
  }
